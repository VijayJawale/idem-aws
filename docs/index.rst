.. idem-aws documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to idem-aws's Documentation!
====================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/idem-aws
   tutorial/index
   releases/index

.. include:: /_includes/reference-toc.rst

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/idem/idem-aws/>

.. toctree::
   :caption: Links

   Idem Project Docs Home <https://docs.idemproject.io>
   Idem Project Website <https://www.idemproject.io>
   Idem Project Repositories on GitLab <https://gitlab.com/vmware/idem>
   POP Project Repositories on GitLab <https://gitlab.com/vmware/pop>
