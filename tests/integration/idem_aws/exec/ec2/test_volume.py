import time
from typing import List

import pytest


@pytest.mark.asyncio
async def test_get(hub, ctx, aws_ec2_volume):
    volume_get_name = "idem-test-exec-get-volume-" + str(int(time.time()))
    tags_filter = {"tag:Name": aws_ec2_volume["tags"]["Name"]}
    ret = await hub.exec.aws.ec2.volume.get(
        ctx,
        name=volume_get_name,
        resource_id=aws_ec2_volume["resource_id"],
        tags=tags_filter,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert volume_get_name == resource["name"]
    assert aws_ec2_volume["resource_id"] == resource["resource_id"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    volume_get_name = "idem-test-exec-get-volume-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.volume.get(
        ctx, name=volume_get_name, resource_id="fake-id,"
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.volume '{volume_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
async def test_list(hub, ctx, aws_ec2_volume):
    volume_list_name = "idem-test-exec-get-volume-" + str(int(time.time()))
    tags_filter = {"tag:Name": aws_ec2_volume["tags"]["Name"]}
    ret = await hub.exec.aws.ec2.volume.list(
        ctx, name=volume_list_name, tags=tags_filter
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) >= 1
    resource = ret["ret"][0]
    assert resource.get("resource_id")
