import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE = "aws.neptune.db_cluster_parameter_group"
PARAMETER = {
    "name": "idem-test-nep-db-cpg-" + str(int(time.time())),
    "db_parameter_group_family": "neptune1",
    "description": "idem-test",
}

comment_utils_kwargs = {"resource_type": RESOURCE_TYPE, "name": PARAMETER["name"]}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test

    # we will use below params for update tests
    # Note: only "parameters" and "tags" are modifiable,
    # we cannot create new parameters for neptune db cluster parameter group
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.neptune.db_cluster_parameter_group.present(
        ctx, **PARAMETER
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        would_create_message = hub.tool.aws.comment_utils.would_create_comment(
            **comment_utils_kwargs
        )[0]
        assert would_create_message in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        created_message = hub.tool.aws.comment_utils.create_comment(
            **comment_utils_kwargs
        )[0]
        assert created_message in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["db_parameter_group_family"] == resource.get(
        "db_parameter_group_family"
    )
    assert PARAMETER["description"] == resource.get("description")
    if not __test and not hub.tool.utils.is_running_localstack(ctx):
        # only in run mode, we will get "parameters" for this resource
        assert resource.get("parameters")
        PARAMETER["parameters"] = resource.get("parameters")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    name = PARAMETER["name"]
    resource_id = PARAMETER["resource_id"]
    ret = await hub.exec.aws.neptune.db_cluster_parameter_group.get(
        ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    get_ret = ret["ret"]
    assert PARAMETER["tags"] == get_ret.get("tags")
    assert PARAMETER["name"] == get_ret.get("name")
    assert PARAMETER["db_parameter_group_family"] == get_ret.get(
        "db_parameter_group_family"
    )
    assert PARAMETER["description"] == get_ret.get("description")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["parameters"] == get_ret.get("parameters")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get-invalid", depends=["exec-get"])
async def test_get_invalid_resource_id(hub, ctx):
    db_clus_param_grp_name = "idem-test-exec-get-nep-cpg-" + str(int(time.time()))
    ret = await hub.exec.aws.neptune.db_cluster_parameter_group.get(
        ctx,
        name=db_clus_param_grp_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    empty_comment = hub.tool.aws.comment_utils.get_empty_comment(
        resource_type=RESOURCE_TYPE, name=db_clus_param_grp_name
    )
    assert empty_comment in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-list", depends=["exec-get-invalid"])
async def test_exec_list(hub, ctx):
    name = PARAMETER["name"]
    ret = await hub.exec.aws.neptune.db_cluster_parameter_group.list(ctx, name=name)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    # we expect at least one during test,
    # there could be more than one if multiple tests are running at same time
    assert len(ret["ret"]) >= 1
    db_cluster_parameter_group = ret["ret"][0]
    assert db_cluster_parameter_group.get("name")
    assert db_cluster_parameter_group.get("resource_id")
    assert db_cluster_parameter_group.get("db_parameter_group_family")
    assert db_cluster_parameter_group.get("description")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert db_cluster_parameter_group.get("parameters")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["exec-list"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.neptune.db_cluster_parameter_group.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert "aws.neptune.db_cluster_parameter_group.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.neptune.db_cluster_parameter_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["name"] == described_resource_map.get("name")
    assert PARAMETER["db_parameter_group_family"] == described_resource_map.get(
        "db_parameter_group_family"
    )
    assert PARAMETER["description"] == described_resource_map.get("description")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert described_resource_map.get("parameters")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_add_tag", depends=["describe"])
async def test_update_add_tag(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    PARAM_NAME = "neptune_query_timeout"
    PARAM_VAL = "121000"
    # validate the parameters we are going to update exists, localstack doesn't support this
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_parameter["parameters"]
        assert (
            len(
                [
                    p
                    for p in new_parameter["parameters"]
                    if p["ParameterName"] == PARAM_NAME
                ]
            )
            == 1
        )
        # create payload for update neptune query timeout value
        for p in new_parameter["parameters"]:
            if p.get("ParameterName") == PARAM_NAME:
                p["ParameterValue"] = PARAM_VAL
                # we need to change below because AWS automatically changes it and we need to validate it in our test
                p["Source"] = "user"
    # update below values
    new_parameter["tags"].update({"Name2": PARAMETER["name"]})

    ret = await hub.states.aws.neptune.db_cluster_parameter_group.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        would_update_message = hub.tool.aws.comment_utils.would_update_comment(
            **comment_utils_kwargs
        )[0]
        assert would_update_message in ret["comment"]
    else:
        update_message = hub.tool.aws.comment_utils.update_comment(
            **comment_utils_kwargs
        )[0]
        assert update_message in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert new_parameter["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["db_parameter_group_family"] == resource.get(
        "db_parameter_group_family"
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_parameter["parameters"] == resource.get("parameters")
    if not __test and not hub.tool.utils.is_running_localstack(ctx):
        PARAMETER["parameters"] = new_parameter["parameters"]
    if not __test:
        PARAMETER["tags"] = new_parameter["tags"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_delete_tag", depends=["update_add_tag"])
async def test_update_delete_tag(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    # reset tag to original value below
    new_parameter["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.neptune.db_cluster_parameter_group.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        would_update_message = hub.tool.aws.comment_utils.would_update_comment(
            **comment_utils_kwargs
        )[0]
        assert would_update_message in ret["comment"]
    else:
        update_message = hub.tool.aws.comment_utils.update_comment(
            **comment_utils_kwargs
        )[0]
        assert update_message in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert new_parameter["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["db_parameter_group_family"] == resource.get(
        "db_parameter_group_family"
    )
    if not __test:
        PARAMETER["tags"] = new_parameter["tags"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update_delete_tag"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.neptune.db_cluster_parameter_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    if __test:
        would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
            **comment_utils_kwargs
        )[0]
        assert would_delete_message in ret["comment"]
    else:
        deleted_message = hub.tool.aws.comment_utils.delete_comment(
            **comment_utils_kwargs
        )[0]
        assert deleted_message in ret["comment"]
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["db_parameter_group_family"] == old_resource.get(
        "db_parameter_group_family"
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.neptune.db_cluster_parameter_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    assert already_absent_message in ret["comment"]
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_already_absent_with_none_resource_id(hub, ctx):
    db_clus_param_grp_name = PARAMETER["name"]
    # Delete neptune db instance with resource_id as None. Result in no-op.
    ret = await hub.states.aws.neptune.db_cluster_parameter_group.absent(
        ctx, name=db_clus_param_grp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    assert already_absent_message in ret["comment"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.neptune.db_cluster_parameter_group.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
