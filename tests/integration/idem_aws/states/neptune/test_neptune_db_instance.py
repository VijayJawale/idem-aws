import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
RESOURCE_TYPE = "aws.neptune.db_instance"
PARAMETER = {
    "name": "idem-test-nep-db-ins-" + str(int(time.time())),
    "db_instance_class": "db.t4g.medium",
    "engine": "neptune",
}

comment_utils_kwargs = {"resource_type": RESOURCE_TYPE, "name": PARAMETER["name"]}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_neptune_db_cluster, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["db_cluster_identifier"] = aws_neptune_db_cluster.get("resource_id")
    # we will use below params for update tests
    PARAMETER["preferred_maintenance_window"] = "fri:08:26-fri:08:56"
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.neptune.db_instance.present(ctx, **PARAMETER)
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        would_create_message = hub.tool.aws.comment_utils.would_create_comment(
            **comment_utils_kwargs
        )[0]
        assert would_create_message in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        created_message = hub.tool.aws.comment_utils.create_comment(
            **comment_utils_kwargs
        )[0]
        assert created_message in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["db_instance_class"] == resource.get("db_instance_class")
    assert PARAMETER["engine"] == resource.get("engine")
    assert PARAMETER["db_cluster_identifier"] == resource.get("db_cluster_identifier")
    # localstack doesn't seem to honor some parameters
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["preferred_maintenance_window"] == resource.get(
            "preferred_maintenance_window"
        )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
async def test_exec_get(hub, ctx):
    name = PARAMETER["name"]
    resource_id = PARAMETER["resource_id"]
    ret = await hub.exec.aws.neptune.db_instance.get(
        ctx, name=name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    get_ret = ret["ret"]
    assert PARAMETER["db_instance_class"] == get_ret.get("db_instance_class")
    assert PARAMETER["engine"] == get_ret.get("engine")
    assert PARAMETER["db_cluster_identifier"] == get_ret.get("db_cluster_identifier")
    assert PARAMETER["tags"] == get_ret.get("tags")
    # localstack doesn't seem to honor some parameters
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["preferred_maintenance_window"] == get_ret.get(
            "preferred_maintenance_window"
        )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get-invalid", depends=["exec-get"])
async def test_get_invalid_resource_id(hub, ctx):
    db_instance_get_name = "idem-test-exec-get-nep-db-ins-" + str(int(time.time()))
    ret = await hub.exec.aws.neptune.db_instance.get(
        ctx,
        name=db_instance_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    empty_comment = hub.tool.aws.comment_utils.get_empty_comment(
        resource_type=RESOURCE_TYPE, name=db_instance_get_name
    )
    assert empty_comment in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-list", depends=["exec-get-invalid"])
async def test_exec_list(hub, ctx):
    name = PARAMETER["name"]
    ret = await hub.exec.aws.neptune.db_instance.list(ctx, name=name)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    # we expect atleast one during test,
    # there could be more than one if multiple tests are running at same time
    assert len(ret["ret"]) >= 1
    db_instance = ret["ret"][0]
    assert db_instance.get("name")
    assert db_instance.get("resource_id")
    assert db_instance.get("db_instance_class")
    assert db_instance.get("engine")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["exec-list"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.neptune.db_instance.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.neptune.db_instance.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.neptune.db_instance.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["db_instance_class"] == described_resource_map.get(
        "db_instance_class"
    )
    assert PARAMETER["engine"] == described_resource_map.get("engine")
    assert PARAMETER["db_cluster_identifier"] == described_resource_map.get(
        "db_cluster_identifier"
    )
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    # localstack doesn't seem to honor some parameters
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["preferred_maintenance_window"] == described_resource_map.get(
            "preferred_maintenance_window"
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_add_tag", depends=["describe"])
async def test_update_add_tag(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    # update below values
    new_parameter["preferred_maintenance_window"] = "fri:09:29-fri:09:59"
    new_parameter["tags"].update({"Name2": PARAMETER["name"]})

    ret = await hub.states.aws.neptune.db_instance.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        would_update_message = hub.tool.aws.comment_utils.would_update_comment(
            **comment_utils_kwargs
        )[0]
        assert would_update_message in ret["comment"]
    else:
        update_message = hub.tool.aws.comment_utils.update_comment(
            **comment_utils_kwargs
        )[0]
        assert update_message in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert new_parameter["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["db_instance_class"] == resource.get("db_instance_class")
    assert PARAMETER["engine"] == resource.get("engine")
    assert PARAMETER["db_cluster_identifier"] == resource.get("db_cluster_identifier")
    # localstack doesn't seem to honor some parameters
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_parameter["preferred_maintenance_window"] == resource.get(
            "preferred_maintenance_window"
        )
    if not __test:
        PARAMETER["tags"] = new_parameter["tags"]
        PARAMETER["preferred_maintenance_window"] = new_parameter[
            "preferred_maintenance_window"
        ]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_delete_tag", depends=["update_add_tag"])
async def test_update_delete_tag(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    # reset tag to original value below
    new_parameter["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.neptune.db_instance.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        would_update_message = hub.tool.aws.comment_utils.would_update_comment(
            **comment_utils_kwargs
        )[0]
        assert would_update_message in ret["comment"]
    else:
        update_message = hub.tool.aws.comment_utils.update_comment(
            **comment_utils_kwargs
        )[0]
        assert update_message in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert new_parameter["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["db_instance_class"] == resource.get("db_instance_class")
    assert PARAMETER["engine"] == resource.get("engine")
    assert PARAMETER["db_cluster_identifier"] == resource.get("db_cluster_identifier")
    # localstack doesn't seem to honor some parameters
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["preferred_maintenance_window"] == resource.get(
            "preferred_maintenance_window"
        )
    if not __test:
        PARAMETER["tags"] = new_parameter["tags"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update_delete_tag"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.neptune.db_instance.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    if __test:
        would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
            **comment_utils_kwargs
        )[0]
        assert would_delete_message in ret["comment"]
    else:
        deleted_message = hub.tool.aws.comment_utils.delete_comment(
            **comment_utils_kwargs
        )[0]
        assert deleted_message in ret["comment"]
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["db_instance_class"] == old_resource.get("db_instance_class")
    assert PARAMETER["engine"] == old_resource.get("engine")
    assert PARAMETER["db_cluster_identifier"] == old_resource.get(
        "db_cluster_identifier"
    )
    # localstack doesn't seem to honor some parameters
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["preferred_maintenance_window"] == old_resource.get(
            "preferred_maintenance_window"
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.neptune.db_instance.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    assert already_absent_message in ret["comment"]
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_already_absent_with_none_resource_id(hub, ctx):
    db_instance_temp_name = PARAMETER["name"]
    # Delete neptune db instance with resource_id as None. Result in no-op.
    ret = await hub.states.aws.neptune.db_instance.absent(
        ctx, name=db_instance_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    assert already_absent_message in ret["comment"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.neptune.db_instance.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
