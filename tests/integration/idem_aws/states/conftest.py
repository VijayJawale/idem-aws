import asyncio
import copy
import time
import uuid
from typing import Any
from typing import Dict

import pytest
import pytest_asyncio


@pytest_asyncio.fixture(scope="module")
async def aws_iam_eks_role(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it for eks service
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-eks-role-" + str(uuid.uuid4())
    assume_role_policy_document = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"Service": "eks.amazonaws.com"},
                "Action": "sts:AssumeRole",
            },
            {
                "Effect": "Allow",
                "Principal": {"Service": "ec2.amazonaws.com"},
                "Action": "sts:AssumeRole",
            },
        ],
    }
    description = "Idem IAM role integration test fixture"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")
    yield after

    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_cluster_role_assignment(hub, ctx, aws_iam_eks_role) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role assignment for a module that needs it for cluster
    :return: a description of an IAM role
    """
    role_name = aws_iam_eks_role.get("name")
    attach_name_1 = "idem-fixture-policy-attachment-" + str(uuid.uuid4())
    attach_name_2 = "idem-fixture-policy-attachment-" + str(uuid.uuid4())

    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_name_1,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_name_2,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSVPCResourceController",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    yield aws_iam_eks_role

    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_name_1,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_name_2,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSVPCResourceController",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_worker_role_assignment(hub, ctx, aws_iam_eks_role) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role assignment for a module that needs it for worker
    :return: a description of an IAM role
    """
    attach_name_1 = "idem-fixture-policy-attachment-" + str(uuid.uuid4())
    attach_name_2 = "idem-fixture-policy-attachment-" + str(uuid.uuid4())
    attach_name_3 = "idem-fixture-policy-attachment-" + str(uuid.uuid4())
    role_name = aws_iam_eks_role.get("name")
    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_name_1,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_name_2,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_name_3,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    yield aws_iam_eks_role

    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_name_1,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_name_2,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_name_3,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_vpc_with_ig(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-fixture-vpc-" + str(uuid.uuid4())
    cidr_block_association_set = [{"CidrBlock": f"10.0.0.0/16"}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=cidr_block_association_set,
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")

    # internet gateway
    internet_gateway_id = "idem-test-internet-gateway-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": internet_gateway_id},
    ]
    vpc_id = after.get("resource_id")
    # When vpc_id passed, resource is created and attached to vpc
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=internet_gateway_id,
        vpc_id=[vpc_id],
        tags=tags,
    )
    created_internet_gateway_id = ret["new_state"]["resource_id"]
    assert (
        hub.tool.aws.tag_utils.convert_tag_list_to_dict(tags)
        == ret["new_state"]["tags"]
    )
    assert vpc_id == ret["new_state"]["vpc_id"][0]
    describe_route_table_ret = await hub.exec.boto3.client.ec2.describe_route_tables(
        ctx,
        DryRun=False,
        Filters=[
            {
                "Name": "vpc-id",
                "Values": [
                    vpc_id,
                ],
            },
        ],
    )
    route_table = describe_route_table_ret["ret"].RouteTables[0]
    route_table_id = route_table["RouteTableId"]
    new_routes = route_table["Routes"]

    copy.deepcopy(new_routes)
    new_routes.append(
        {
            "DestinationCidrBlock": "0.0.0.0/0",
            "GatewayId": created_internet_gateway_id,
        }
    )
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_id,
        resource_id=route_table_id,
        vpc_id=vpc_id,
        tags=[{"Key": "Name", "Value": vpc_id}],
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=created_internet_gateway_id, resource_id=created_internet_gateway_id
    )
    assert f"Deleted '{created_internet_gateway_id}'" in ret["comment"]

    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_subnet_for_eks_worker(
    hub, ctx, aws_ec2_vpc_with_ig
) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(uuid.uuid4())
    az = ctx["acct"].get("region_name") + "a"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc_with_ig["cidr_block_association_set"][0]["CidrBlock"], 24
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc_with_ig.get("resource_id"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": "aws_ec2_subnet_for_eks_worker"}],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    result = await hub.exec.boto3.client.ec2.modify_subnet_attribute(
        ctx, SubnetId=resource_id, MapPublicIpOnLaunch={"Value": True}
    )
    assert result["result"], result["comment"]

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_subnet_for_eks(hub, ctx, aws_ec2_vpc_with_ig) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(uuid.uuid4())
    az = ctx["acct"].get("region_name") + "b"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc_with_ig["cidr_block_association_set"][0]["CidrBlock"], 24
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc_with_ig.get("resource_id"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": "aws_ec2_subnet_for_eks"}],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.localstack(
    False,
    "localstack is unable to create EKS resources, so test against a real AWS endpoint",
)
@pytest_asyncio.fixture(scope="module")
async def aws_eks_cluster(
    hub,
    ctx,
    aws_iam_cluster_role_assignment,
    aws_ec2_subnet_for_eks_worker,
    aws_ec2_subnet_for_eks,
    aws_security_group,
):
    arn = aws_iam_cluster_role_assignment["arn"]
    cluster_name = "idem-test-cluster-" + str(int(time.time()))
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version="1.21",
        resources_vpc_config={
            "endpointPrivateAccess": True,
            "endpointPublicAccess": False,
            "subnetIds": [
                aws_ec2_subnet_for_eks_worker.get("resource_id"),
                aws_ec2_subnet_for_eks.get("resource_id"),
            ],
            "securityGroupIds": [aws_security_group.get("resource_id")],
        },
        kubernetes_network_config={
            "ipFamily": "ipv4",
            "serviceIpv4Cidr": "172.20.0.0/16",
        },
        tags={"Name": cluster_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.eks.cluster '{cluster_name}'" in ret["comment"]
    resource = ret.get("new_state")
    cluster_id = resource.get("name")

    yield resource

    # Delete cluster
    ret = await hub.states.aws.eks.cluster.absent(
        ctx, name=cluster_id, resource_id=cluster_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.eks.cluster '{cluster_id}'" in ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_security_group(hub, ctx, aws_ec2_vpc_with_ig) -> Dict[str, Any]:
    name = "idem-fixture-security-group-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": name},
    ]
    ret = await hub.states.aws.ec2.security_group.present(
        ctx=ctx,
        name=name,
        description=f"Created for Idem integration test.",
        vpc_id=aws_ec2_vpc_with_ig.get("resource_id"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert name == ret["name"]
    created_security_group_id = ret["new_state"]["resource_id"]

    yield ret["new_state"]

    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=name, resource_id=created_security_group_id
    )
    assert f"Deleted aws.ec2.security_group '{name}'" in ret["comment"]


@pytest.mark.localstack(
    False,
    "localstack is unable to create EKS resources, so test against a real AWS endpoint",
)
@pytest_asyncio.fixture(scope="module")
async def aws_eks_cluster_fargate(
    hub,
    ctx,
    aws_iam_cluster_role_assignment,
    aws_security_group,
    aws_ec2_private_subnet_for_eks,
    aws_ec2_private_subnet2_for_eks,
):
    arn = aws_iam_cluster_role_assignment["arn"]
    cluster_name = "idem-fixture-cluster-" + str(uuid.uuid4())
    ret = await hub.states.aws.eks.cluster.present(
        ctx,
        name=cluster_name,
        role_arn=arn,
        version="1.21",
        resources_vpc_config={
            "endpointPrivateAccess": True,
            "endpointPublicAccess": False,
            "subnetIds": [
                aws_ec2_private_subnet_for_eks.get("resource_id"),
                aws_ec2_private_subnet2_for_eks.get("resource_id"),
            ],
            "securityGroupIds": [aws_security_group.get("resource_id")],
        },
        kubernetes_network_config={
            "ipFamily": "ipv4",
            "serviceIpv4Cidr": "172.20.0.0/16",
        },
        tags={"Name": cluster_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.eks.cluster '{cluster_name}'" in ret["comment"]
    resource = ret.get("new_state")
    cluster_id = resource.get("name")

    yield resource

    # Delete cluster
    ret = await hub.states.aws.eks.cluster.absent(
        ctx, name=cluster_id, resource_id=cluster_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.eks.cluster '{cluster_id}'" in ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_private_subnet_for_eks(
    hub, ctx, aws_ec2_vpc_with_ig
) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 private subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(uuid.uuid4())
    az = ctx["acct"].get("region_name") + "b"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc_with_ig["cidr_block_association_set"][0]["CidrBlock"], 24
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc_with_ig.get("resource_id"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": "aws_ec2_private_subnet_for_eks"}],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")
    subnet_id = after.get("resource_id")

    nat_gateway_temp_name = "idem-fixture-ng-" + str(uuid.uuid4())
    nat_ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_temp_name,
        subnet_id=subnet_id,
        connectivity_type="private",
        tags=[{"Key": "Name", "Value": "aws_ec2_nat_gateway"}],
    )
    assert nat_ret["result"], nat_ret["comment"]
    assert nat_ret.get("new_state")
    resource_id = nat_ret.get("new_state").get("resource_id")

    vpc_id = aws_ec2_vpc_with_ig.get("resource_id")

    describe_route_table_ret = await hub.exec.boto3.client.ec2.describe_route_tables(
        ctx,
        DryRun=False,
        Filters=[
            {
                "Name": "vpc-id",
                "Values": [
                    vpc_id,
                ],
            },
        ],
    )
    route_table = describe_route_table_ret["ret"].RouteTables[0]
    route_table_id = route_table["RouteTableId"]
    new_routes = route_table.get("Routes")
    new_routes[1]["GatewayId"] = resource_id
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_id,
        resource_id=route_table_id,
        vpc_id=vpc_id,
        tags=[{"Key": "Name", "Value": vpc_id}],
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.nat_gateway.absent(
        ctx, name=nat_gateway_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=subnet_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_private_subnet2_for_eks(
    hub, ctx, aws_ec2_vpc_with_ig
) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 private subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(uuid.uuid4())
    az = ctx["acct"].get("region_name") + "c"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc_with_ig["cidr_block_association_set"][0]["CidrBlock"], 24
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc_with_ig.get("resource_id"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": "aws_ec2_private_subnet2_for_eks"}],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")
    subnet_id = after.get("resource_id")

    nat_gateway_temp_name = "idem-fixture-ng-" + str(uuid.uuid4())
    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_temp_name,
        subnet_id=subnet_id,
        connectivity_type="private",
        tags=[{"Key": "Name", "Value": "aws_ec2_nat_gateway2"}],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    resource_id = ret.get("new_state").get("resource_id")

    vpc_id = aws_ec2_vpc_with_ig.get("resource_id")

    describe_route_table_ret = await hub.exec.boto3.client.ec2.describe_route_tables(
        ctx,
        DryRun=False,
        Filters=[
            {
                "Name": "vpc-id",
                "Values": [
                    vpc_id,
                ],
            },
        ],
    )
    route_table = describe_route_table_ret["ret"].RouteTables[0]
    route_table_id = route_table["RouteTableId"]
    new_routes = route_table.get("Routes")
    new_routes[1]["GatewayId"] = resource_id
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_id,
        resource_id=route_table_id,
        vpc_id=vpc_id,
        tags=[{"Key": "Name", "Value": vpc_id}],
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.nat_gateway.absent(
        ctx, name=nat_gateway_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=subnet_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_fargate_role_assignment(
    hub, ctx, aws_iam_eks_fargate_role
) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role assignment for a module that needs it for fargate profile
    :return: a description of an IAM role
    """
    role_name = aws_iam_eks_fargate_role.get("name")
    attach_name_1 = "idem-fixture-policy-attachment-" + str(uuid.uuid4())
    policy_arn = "arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy"

    ret = await hub.states.aws.iam.role_policy_attachment.present(
        ctx,
        name=attach_name_1,
        role_name=role_name,
        policy_arn=policy_arn,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    # TODO: A hard-code sleep is not a good practice. This need to be fixed later with proper waiter implementation.
    await asyncio.sleep(60)
    yield aws_iam_eks_fargate_role

    ret = await hub.states.aws.iam.role_policy_attachment.absent(
        ctx,
        name=attach_name_1,
        role_name=role_name,
        policy_arn="arn:aws:iam::aws:policy/AmazonEKSFargatePodExecutionRolePolicy",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_eks_fargate_role(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM fargate role for a module that needs it for eks service
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-fg-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": [{"Effect": "Allow","Principal": {"Service": ["eks-fargate-pods.amazonaws.com"]},"Action": "sts:AssumeRole"}]}'
    description = "Idem IAM Fargate role integration test fixture"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.role.absent(ctx, name=after["name"])
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_snapshot(hub, ctx) -> str:
    snapshot_ids = await hub.exec.boto3.client.ec2.describe_snapshots(
        ctx, OwnerIds=["self"]
    )
    snapshot_id = None
    if snapshot_ids["ret"].get("Snapshots"):
        snapshot_id = snapshot_ids["ret"].get("Snapshots")[0].get("SnapshotId")

    new_snapshot_created = False
    if not snapshot_id:
        vol_ret = await hub.exec.boto3.client.ec2.create_volume(
            ctx,
            AvailabilityZone=ctx["acct"].get("region_name") + "a",
            Encrypted=False,
            Size=1,
            VolumeType="gp3",
        )
        assert vol_ret["result"], vol_ret["comment"]
        volume_id = vol_ret["ret"].get("VolumeId")

        await hub.tool.boto3.client.wait(
            ctx, "ec2", "volume_available", VolumeIds=[volume_id]
        )

        ret = await hub.exec.boto3.client.ec2.create_snapshot(ctx, VolumeId=volume_id)
        assert ret["result"], ret["comment"]
        snapshot_id = ret["ret"]["SnapshotId"]
        await hub.tool.boto3.client.wait(
            ctx, "ec2", "snapshot_completed", SnapshotIds=[snapshot_id]
        )
        new_snapshot_created = True

    yield snapshot_id

    if new_snapshot_created:
        snap_ret = await hub.exec.boto3.client.ec2.delete_snapshot(
            ctx, SnapshotId=snapshot_id
        )
        assert snap_ret["result"], snap_ret["comment"]
        vol_ret = await hub.exec.boto3.client.ec2.delete_volume(ctx, VolumeId=volume_id)
        assert vol_ret["result"], vol_ret["comment"]


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False, "localstack doesn't support efs file system creation.")
async def aws_efs_file_system(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-file-system-" + str(uuid.uuid4())
    creation_token = str(uuid.uuid4())
    performance_mode = "generalPurpose"
    encrypted = False
    throughput_mode = "bursting"
    availability_zone_name = ctx["acct"].get("region_name") + "b"
    backup = False
    tags = [{"Key": "Name", "Value": name}]
    ret = await hub.states.aws.efs.file_system.present(
        ctx=ctx,
        name=name,
        creation_token=creation_token,
        performance_mode=performance_mode,
        encrypted=encrypted,
        throughput_mode=throughput_mode,
        availability_zone_name=availability_zone_name,
        backup=backup,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert name == ret["name"]
    created_file_system_id = ret["new_state"]["resource_id"]

    yield ret["new_state"]

    ret = await hub.states.aws.efs.file_system.absent(
        ctx, name=name, resource_id=created_file_system_id
    )
    assert f"Deleted aws.efs.file_system '{name}'" in ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_security_group_2(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 security group for a module that needs it
    :return: a description of an ec2 security group
    """
    security_group_temp_name = "idem-fixture-security-group-" + str(uuid.uuid4())
    security_group_tags = [{"Key": "Name", "Value": security_group_temp_name}]
    description = "Security group fixture resource for Idem integration test."
    ret = await hub.states.aws.ec2.security_group.present(
        ctx,
        name=security_group_temp_name,
        description=description,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=security_group_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=security_group_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_dead_letter_sqs_queue_arn(hub, ctx):
    """
    Create and clean up a standard dead-letter sqs queue
    :return: the queue arn
    """

    name = "idem-test-dead-letter-queue-" + str(int(time.time()))
    ret = await hub.states.aws.sqs.queue.present(ctx, name=name)

    assert ret["result"], ret["comment"]
    assert f"Created aws.sqs.queue '{name}'" in ret["comment"]
    new_state = ret["new_state"]
    assert new_state

    resource_id = new_state.get("resource_id")
    resource = hub.tool.boto3.resource.create(ctx, "sqs", "Queue", resource_id)
    resource_description = await hub.tool.boto3.resource.describe(resource)
    assert resource_description.get("Attributes")
    arn = resource_description["Attributes"].get("QueueArn")
    assert arn
    yield arn

    ret = await hub.states.aws.sqs.queue.absent(ctx, name, resource_id)
    assert ret["result"], ret["comment"]
    assert ret["old_state"], not ret["new_state"]
    assert f"Deleted aws.sqs.queue '{name}'" in ret["comment"]


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(pro=True)
async def aws_neptune_db_cluster(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-nep-clus-" + str(int(time.time()))
    engine = "neptune"
    deletion_protection = False
    ret = await hub.states.aws.neptune.db_cluster.present(
        ctx, name=name, engine=engine, deletion_protection=deletion_protection
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.neptune.db_cluster.absent(
        ctx, name=name, resource_id=after["resource_id"], skip_final_snapshot=True
    )
