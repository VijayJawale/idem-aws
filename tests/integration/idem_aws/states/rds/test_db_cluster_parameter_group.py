import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_db_cluster_parameter_group(hub, ctx):
    # Create DB Cluster Parameter Group with test flag
    db_cluster_parameter_group_temp_name = (
        "idem-test-db-cluster-parameter-group-" + str(uuid.uuid4())
    )
    tags = {"Name": db_cluster_parameter_group_temp_name}
    parameters = [
        {
            "ParameterName": "aurora_disable_hash_join",
            "ParameterValue": "1",
            "ApplyMethod": "immediate",
        }
    ]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.rds.db_cluster_parameter_group.present(
        test_ctx,
        name=db_cluster_parameter_group_temp_name,
        db_parameter_group_family="aurora5.6",
        description="For testing idem plugin",
        tags=tags,
        parameters=parameters,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.rds.db_cluster_parameter_group",
            name=db_cluster_parameter_group_temp_name,
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert db_cluster_parameter_group_temp_name == resource.get("name")
    assert "aurora5.6" == resource.get("db_parameter_group_family")
    assert "For testing idem plugin" == resource.get("description")

    # Create DB Cluster Parameter Group
    ret = await hub.states.aws.rds.db_cluster_parameter_group.present(
        ctx,
        name=db_cluster_parameter_group_temp_name,
        db_parameter_group_family="aurora5.6",
        description="For testing idem plugin",
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.rds.db_cluster_parameter_group",
            name=db_cluster_parameter_group_temp_name,
        )[0]
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert db_cluster_parameter_group_temp_name == resource.get("name")
    assert "aurora5.6" == resource.get("db_parameter_group_family")
    assert "For testing idem plugin" == resource.get("description")

    # Describe DB Cluster Parameter Group
    resource = ret["new_state"]
    ret_db_cluster_parameter_group_name = resource.get("resource_id")
    describe_ret = await hub.states.aws.rds.db_cluster_parameter_group.describe(ctx)
    assert ret_db_cluster_parameter_group_name in describe_ret
    # Verify that describe output format is correct
    assert "aws.rds.db_cluster_parameter_group.present" in describe_ret.get(
        ret_db_cluster_parameter_group_name
    )
    described_resource = describe_ret.get(ret_db_cluster_parameter_group_name).get(
        "aws.rds.db_cluster_parameter_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "resource_id" in described_resource_map
    assert "db_parameter_group_family" in described_resource_map
    assert "description" in described_resource_map
    assert tags == described_resource_map.get("tags")
    assert db_cluster_parameter_group_temp_name == described_resource_map.get(
        "resource_id"
    )
    assert "aurora5.6" == resource.get("db_parameter_group_family")
    assert "For testing idem plugin" == resource.get("description")

    # Update DB Cluster Parameter Group with test flag
    tags.update(
        {
            f"idem-test-db-cluster-parameter-group-key-{str(uuid.uuid4())}": f"idem-test-db-cluster-parameter-group-value-{str(uuid.uuid4())}",
        }
    )
    ret = await hub.states.aws.rds.db_cluster_parameter_group.present(
        test_ctx,
        name=db_cluster_parameter_group_temp_name,
        resource_id=db_cluster_parameter_group_temp_name,
        db_parameter_group_family="aurora5.6",
        description="For testing idem plugin",
        tags=tags,
        parameters=parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert db_cluster_parameter_group_temp_name == resource.get("resource_id")
    assert tags == resource.get("tags")

    # Update DB Cluster Parameter Group
    ret = await hub.states.aws.rds.db_cluster_parameter_group.present(
        ctx,
        name=db_cluster_parameter_group_temp_name,
        resource_id=db_cluster_parameter_group_temp_name,
        db_parameter_group_family="aurora5.6",
        description="For testing idem plugin",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert db_cluster_parameter_group_temp_name == resource.get("resource_id")

    # Delete db_cluster_parameter_group with test flag
    ret = await hub.states.aws.rds.db_cluster_parameter_group.absent(
        test_ctx,
        name=ret_db_cluster_parameter_group_name,
        resource_id=ret_db_cluster_parameter_group_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.rds.db_cluster_parameter_group",
            name=db_cluster_parameter_group_temp_name,
        )[0]
        in ret["comment"]
    )
    old_resource = ret.get("old_state")
    assert ret_db_cluster_parameter_group_name == old_resource.get("resource_id")
    assert "For testing idem plugin" == old_resource.get("description")

    # Delete db_cluster_parameter_group
    ret = await hub.states.aws.rds.db_cluster_parameter_group.absent(
        ctx,
        name=ret_db_cluster_parameter_group_name,
        resource_id=ret_db_cluster_parameter_group_name,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.rds.db_cluster_parameter_group",
            name=db_cluster_parameter_group_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    old_resource = ret.get("old_state")
    assert db_cluster_parameter_group_temp_name == old_resource.get("resource_id")
    assert "For testing idem plugin" == old_resource.get("description")

    # Deleting db_cluster_parameter_group again should be a no-op
    ret = await hub.states.aws.rds.db_cluster_parameter_group.absent(
        ctx,
        name=ret_db_cluster_parameter_group_name,
        resource_id=ret_db_cluster_parameter_group_name,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.rds.db_cluster_parameter_group",
            name=db_cluster_parameter_group_temp_name,
        )[0]
        in ret["comment"]
    )

    # Delete db_cluster_parameter_group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.rds.db_cluster_parameter_group.absent(
        ctx,
        name=ret_db_cluster_parameter_group_name,
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.rds.db_cluster_parameter_group",
            name=db_cluster_parameter_group_temp_name,
        )[0]
        in ret["comment"]
    )
