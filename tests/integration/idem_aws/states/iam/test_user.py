import copy
import time
import uuid
from collections import ChainMap

import pytest
from Cryptodome.PublicKey import RSA


@pytest.mark.asyncio
async def test_user(hub, ctx):
    # Create IAM user
    user_temp_name = "idem-test-user-" + str(int(time.time()))
    tags = {"Name": user_temp_name}

    # Create IAM user with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.user.present(
        test_ctx, name=user_temp_name, user_name=user_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.iam.user '{user_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert user_temp_name == resource.get("user_name")
    assert "arn_known_after_present" == resource.get("arn")

    ret = await hub.states.aws.iam.user.present(
        ctx, name=user_temp_name, user_name=user_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert user_temp_name == resource.get("user_name")
    assert "/" == resource.get("path")

    resource_id = resource.get("resource_id")

    # Describe IAM User
    describe_ret = await hub.states.aws.iam.user.describe(ctx)
    resource_key = f"iam-user-{resource_id}"
    assert resource_key in describe_ret

    # Verify that the describe output format is correct
    assert "aws.iam.user.present" in describe_ret.get(resource_key)
    described_resource = describe_ret[resource_key].get("aws.iam.user.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert user_temp_name == described_resource_map.get("name")
    assert tags == described_resource_map.get("tags")
    assert user_temp_name == described_resource_map.get("user_name")
    assert resource_id == described_resource_map.get("resource_id")
    assert "/" == described_resource_map.get("path")
    assert "arn" in described_resource_map

    new_user_name = "idem-test-user-" + str(uuid.uuid4())
    tags.update(
        {
            f"idem-test-user-key-{str(uuid.uuid4())}": f"idem-test-user-value-{str(uuid.uuid4())}"
        }
    )
    # update with test flag
    ret = await hub.states.aws.iam.user.present(
        test_ctx,
        name=user_temp_name,
        resource_id=resource_id,
        user_name=new_user_name,
        path="/idem/aws/",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update aws.iam.user '{user_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert new_user_name == resource.get("user_name")
    assert "/idem/aws/" == resource.get("path")

    # test updating only tags with localstack as user_name and path cannot be updated with local stack
    ret = await hub.states.aws.iam.user.present(
        ctx,
        name=user_temp_name,
        resource_id=resource_id,
        user_name=user_temp_name,
        path="/",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert user_temp_name == resource.get("user_name")
    assert "/" == resource.get("path")
    resource_id = resource.get("resource_id")

    # Localstack is not supporting updating username and path. so using real AWS to test.
    username = user_temp_name
    if not hub.tool.utils.is_running_localstack(ctx):
        # test updating user_name and path in real
        ret = await hub.states.aws.iam.user.present(
            ctx,
            name=user_temp_name,
            resource_id=resource_id,
            user_name=new_user_name,
            path="/idem/aws/",
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret.get("new_state")
        assert user_temp_name == resource.get("name")
        assert tags == resource.get("tags")
        assert new_user_name == resource.get("user_name")
        assert "/idem/aws/" == resource.get("path")
        resource_id = resource.get("resource_id")
        username = new_user_name

    # upload ssh keys to user
    key = RSA.generate(2048)
    ssh_public_key_body = str(key.publickey().exportKey("OpenSSH").decode("utf-8"))
    ssh_public_key_id_1 = str(uuid.uuid4())

    # upload first ssh public key to IAM user
    ret = await hub.states.aws.iam.user_ssh_key.present(
        ctx,
        name=ssh_public_key_id_1,
        user_name=username,
        ssh_public_key_body=ssh_public_key_body,
    )

    assert (
        f"Uploaded the ssh public key and attached it to iam user '{username}'"
        in ret["comment"]
    )
    key_1 = ret["new_state"]["resource_id"]

    key = RSA.generate(2048)
    ssh_public_key_body = str(key.publickey().exportKey("OpenSSH").decode("utf-8"))
    ssh_public_key_id_2 = str(uuid.uuid4())

    # upload second ssh public key to IAM user
    ret = await hub.states.aws.iam.user_ssh_key.present(
        ctx,
        name=ssh_public_key_id_2,
        user_name=username,
        ssh_public_key_body=ssh_public_key_body,
    )
    assert (
        f"Uploaded the ssh public key and attached it to iam user '{username}'"
        in ret["comment"]
    )
    key_2 = ret["new_state"]["resource_id"]

    # Delete with test flag
    ret = await hub.states.aws.iam.user.absent(
        test_ctx, name=username, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Would delete aws.iam.user '{username}'" in ret["comment"]

    # Delete IAM user
    ret = await hub.states.aws.iam.user.absent(
        ctx, name=username, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Check if the first ssh key is deleted along with user deletion
    ret_ssh_public_key_1 = await hub.exec.boto3.client.iam.get_ssh_public_key(
        ctx=ctx,
        UserName=username,
        SSHPublicKeyId=key_1,
        Encoding="SSH",
    )
    assert not ret_ssh_public_key_1["result"]

    # Check if the second ssh key is deleted along with user deletion
    ret_ssh_public_key_1 = await hub.exec.boto3.client.iam.get_ssh_public_key(
        ctx=ctx,
        UserName=username,
        SSHPublicKeyId=key_2,
        Encoding="SSH",
    )
    assert not ret_ssh_public_key_1["result"]

    # Delete IAM user again
    ret = await hub.states.aws.iam.user.absent(
        ctx, name=username, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and not (ret["new_state"])
    assert f"'{username}' already absent" in ret["comment"]
